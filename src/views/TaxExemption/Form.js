import React, { Component } from "react";
import {
  CFormGroup,
  CLabel,
  CInput,
  CCol,
  CFormText,
  CInputFile,
  CButton,
  CRow,
  CInputRadio,
  CSelect,
  CProgress,
  CNavLink,
} from "@coreui/react";
import api from "src/services/api";
import Swal from "sweetalert2";

const column = {
  border: "1px solid black",
  padding: 10,
};
const progressBar = {
  height: 20,
  border: "1px solid black",
  backgroundColor: "#e0e0de",
  borderRadius: 50,
};

const table = {
  backgroundColor: "white",
};
class TaxRequestForm extends Component {
  _isMounted = false;
  constructor() {
    super();
    this.state = {
      name: "",
      phoneNumber: "",
      email: "",
      addressLine1: "",
      addressLine2: "",
      city: "",
      stateId: 0,
      states: [],
      postalCode: "",
      shippingAddress1: "",
      shippingAddress2: "",
      shippingCity: "",
      shippingStateId: 0,
      shippingPostalCode: "",
      shippingId: false,
      donorId: "",
      progress: 0,
      amountTest: [],
      filePathTest: [],
      totalAmount: 0,
      refDonation: "",
      amount: 0,
      filePath: null,
      typeId: 0,
      types: [],
      dataType: "",
      nameDataType: "",
      lengthTypes: 0,
      postId: 2,
      posts: [],
      modalForm: false,
      submitForm: false,
      loading: false,
      activeKey: 1,
    };

    this.changeType = this.changeType.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChecked = this.handleChecked.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getAllState();
    this.getAllTypeOfTax();
    this.getAllPostage();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }
  modalForm() {
    this.setState({
      modalForm: !this.state.modalForm,
    });
    this.setForm();
  }

  getAllState() {
    api.get("/api/allState").then((response) => {
      this.setState({
        states: response.data,
      });
    });
  }

  getAllTypeOfTax() {
    api.get("/api/getTypeTax").then((response) => {
      this.setState({
        types: response.data,
        lengthTypes: response.data.length,
        dataType: response.data[0].dataType,
      });
    });
  }

  getAllPostage() {
    api.get("/api/getPostage").then((response) => {
      this.setState({
        posts: response.data,
      });
    });
  }

  changeType = (e) => {
    e.stopPropagation();
    this.changeStateType(e);
  };

  changeStateType(e) {
    this.setState({ typeId: e.target.value }, () =>
      this.dataType(this.state.typeId)
    );
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleChecked = (event) => {
    if (event.target.value === "true") {
      this.setState({
        shippingId: true,
        shippingAddress1: this.state.addressLine1,
        shippingAddress2: this.state.addressLine2,
        shippingCity: this.state.city,
        shippingPostalCode: this.state.postalCode,
        shippingStateId: this.state.stateId,
      });
    } else {
      this.setState({
        shippingId: false,
        shippingAddress1: "",
        shippingAddress2: "",
        shippingCity: "",
        shippingPostalCode: "",
        shippingStateId: "",
      });
    }
  };

  dataType(id) {
    for (var i = 0; i < this.state.lengthTypes; i++) {
      const currentId = this.state.types[i].id;
      // eslint-disable-next-line
      if (id == currentId) {
        const currentDataType = this.state.types[i].dataType;
        const currentNameDataType = this.state.types[i].name;
        this.setState({
          dataType: currentDataType,
          nameDataType: currentNameDataType,
        });
      }
    }
  }

  setForm() {
    this.setState({
      submitForm: false,
      name: "",
      phoneNumber: "",
      email: "",
      addressLine1: "",
      addressLine2: "",
      city: "",
      postalCode: "",
      shippingAddress1: "",
      shippingAddress2: "",
      shippingCity: "",
      shippingStateId: 0,
      shippingPostalCode: "",
      identificationNumber: "",
      amount: 0,
      companyNo: "",
      filePath: "",
      typeId: 0,
      postId: 2,
      stateId: 0,
      shippingId: false,
      donorId: "",
      loading: false,
    });
  }

  nextButton() {
    this.setState({
      activeKey: this.state.activeKey + 1,
      progress: this.state.progress + 1,
    });
  }

  previousButton() {
    this.setState({
      activeKey: this.state.activeKey - 1,
    });
  }

  setActiveKey(key) {
    this.setState({
      activeKey: key,
    });
  }

  submitForm() {
    let timerInterval;
    Swal.fire({
      title: "Loading...",
      timer: 2000,
      didOpen: () => {
        Swal.showLoading();
      },
      willClose: () => {
        clearInterval(timerInterval);
      },
    });
    api.get("/sanctum/csrf-cookie").then(() => {
      let file = this.state.filePath;
      let formData = new FormData();
      formData.append("name", this.state.name);
      formData.append("phoneNumber", this.state.phoneNumber);
      formData.append("email", this.state.email);
      formData.append(
        "address",
        this.state.addressLine1 + "" + this.state.addressLine2
      );
      formData.append("city", this.state.city);
      formData.append("stateId", this.state.stateId);
      formData.append("postalCode", this.state.postalCode);
      formData.append("shippingId", this.state.shippingId);
      formData.append(
        "shippingAddress",
        this.state.shippingAddress1 + "" + this.state.shippingAddress2
      );
      formData.append("shippingCity", this.state.shippingCity);
      formData.append("shippingStateId", this.state.shippingStateId);
      formData.append("shippingPostalCode", this.state.shippingPostalCode);
      formData.append("donorId", this.state.donorId);
      formData.append("amount", this.state.amount);
      formData.append("filePath", file);
      formData.append("typeId", this.state.typeId);
      formData.append("postId", this.state.postId);
      this.modalForm();
      api
        .post("/api/storeTax", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        })
        .then((res) => {
          Swal.fire({
            icon: "success",
            title: "Thank You !",
            text: "Your request was submitted sucessfully",
            showConfirmButton: false,
            timer: 3000,
          });
        })
        .catch((error) => {
          Swal.fire({
            title: "Error",
            text: error,
            icon: "error",
            confirmButtonColor: "#39f",
          });
        });
    });
  }

  render() {
    const {
      name,
      phoneNumber,
      email,
      addressLine1,
      addressLine2,
      city,
      amount,
      // amountTest,
      // totalAmount,
      // filePathTest,
      typeId,
      postalCode,
      stateId,
      dataType,
      nameDataType,
      donorId,
      shippingAddress1,
      shippingAddress2,
      shippingCity,
      shippingStateId,
      shippingPostalCode,
      postId,
      shippingId,
      activeKey,
      progress,
    } = this.state;

    return (
      <div className="containter">
        <CRow className="justify-content-center" md={{ gutterX: 5 }}>
          <CCol>
            <h3>Request Tax Exemption Form</h3>

            <div>
              <h6>
                Thank you for your willingness to contribute in helping Cinta
                Syria Malaysia. For the purpose of obtaining and processing tax
                exemption receipts, you are invited to fill in the following
                information
              </h6>
            </div>
            <hr></hr>
            <CProgress
              value={progress}
              max={4}
              showPercentage
              className="mb-3"
              style={progressBar}
            />
            <CRow>
              <CCol md={3}>
                <CNavLink
                  className="row justify-content-md-center text-center"
                  href="#"
                  onClick={this.setActiveKey.bind(this, 1)}
                >
                  Type
                </CNavLink>
              </CCol>
              <CCol md={3}>
                <CNavLink
                  className="row justify-content-md-center text-center"
                  onClick={this.setActiveKey.bind(this, 2)}
                >
                  Donor Information
                </CNavLink>
              </CCol>
              <CCol md={3}>
                <CNavLink
                  className="row justify-content-md-center text-center"
                  onClick={this.setActiveKey.bind(this, 3)}
                >
                  Donation
                </CNavLink>
              </CCol>
              <CCol md={3}>
                <CNavLink
                  className="row justify-content-md-center text-center"
                  onClick={this.setActiveKey.bind(this, 4)}
                >
                  Others
                </CNavLink>
              </CCol>
              <CCol style={{ paddingTop: 20 }}>
                {activeKey === 1 && (
                  <div>
                    {" "}
                    <div className="row justify-content-md-center">
                      {" "}
                      <CCol md={6}>
                        {" "}
                        <CFormGroup>
                          <CLabel htmlFor="type">
                            Please select type of Tax Exemption
                          </CLabel>
                          <CSelect
                            custom
                            name="typeId"
                            id="typeId"
                            value={typeId}
                            onChange={this.changeType}
                          >
                            {" "}
                            <option value="0">Please select</option>
                            {this.state.types.map((type) => (
                              <option value={type.id} key={type.id}>
                                {" "}
                                {type.type}
                              </option>
                            ))}
                          </CSelect>
                          <CFormText>
                            Please select 1 before proceed to the next step
                          </CFormText>
                        </CFormGroup>
                      </CCol>
                    </div>
                    <CButton
                      color="info"
                      onClick={this.nextButton.bind(this)}
                      className="float-right"
                      disabled={typeId <= 0}
                    >
                      Next
                    </CButton>
                  </div>
                )}
                {activeKey === 2 && (
                  <div>
                    {" "}
                    <CFormGroup>
                      <CLabel htmlFor="name">{nameDataType}</CLabel>
                      <CInput
                        type="text"
                        id="name"
                        name="name"
                        placeholder={nameDataType}
                        value={name}
                        onChange={this.handleChange}
                      />
                      {/* <CFormText>
                    Please ensure your name is in accordance to your
                    Identification Card
                  </CFormText> */}
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel htmlFor="donorId">{dataType}</CLabel>
                      <CInput
                        id="donorId"
                        name="donorId"
                        placeholder={dataType}
                        value={donorId}
                        onChange={this.handleChange}
                      />
                    </CFormGroup>
                    <CFormGroup row className="my-0">
                      <CCol sm="6">
                        <CFormGroup>
                          <CLabel htmlFor="phoneNumber">Phone</CLabel>
                          <CInput
                            id="phoneNumber"
                            name="phoneNumber"
                            placeholder="Phone"
                            value={phoneNumber}
                            onChange={this.handleChange}
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol sm="6">
                        <CFormGroup>
                          <CLabel htmlFor="email">Email</CLabel>
                          <CInput
                            id="email"
                            name="email"
                            placeholder="Email"
                            value={email}
                            onChange={this.handleChange}
                          />
                        </CFormGroup>
                      </CCol>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel htmlFor="address">Address</CLabel>
                      <CInput
                        id="addressLine1"
                        placeholder="Address Line 1"
                        name="addressLine1"
                        value={addressLine1}
                        onChange={this.handleChange}
                      />
                      <br></br>
                      <CInput
                        id="addressLine2"
                        placeholder="Address Line 2"
                        name="addressLine2"
                        value={addressLine2}
                        onChange={this.handleChange}
                      />
                    </CFormGroup>
                    <CFormGroup row className="my-0">
                      <CCol sm="4">
                        <CFormGroup>
                          <CLabel htmlFor="city">City</CLabel>
                          <CInput
                            id="city"
                            name="city"
                            placeholder="City"
                            value={city}
                            onChange={this.handleChange}
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol sm="4">
                        <CFormGroup>
                          <CLabel htmlFor="state">State</CLabel>
                          <CSelect
                            name="stateId"
                            id="state"
                            value={stateId}
                            onChange={this.handleChange}
                          >
                            <option value="0">Please select</option>
                            {this.state.states.map((state) => (
                              <option value={state.id} key={state.id}>
                                {" "}
                                {state.state}
                              </option>
                            ))}
                          </CSelect>
                        </CFormGroup>
                      </CCol>
                      <CCol sm="4">
                        <CFormGroup>
                          <CLabel htmlFor="postalCode">Postal Code</CLabel>
                          <CInput
                            id="postalCode"
                            placeholder="Postal Code"
                            name="postalCode"
                            value={postalCode}
                            onChange={this.handleChange}
                          />
                        </CFormGroup>
                      </CCol>
                    </CFormGroup>
                    <CButton
                      color="secondary"
                      onClick={this.previousButton.bind(this)}
                      className="float-left"
                    >
                      Previous
                    </CButton>
                    <CButton
                      color="info"
                      onClick={this.nextButton.bind(this)}
                      className="float-right"
                    >
                      Next
                    </CButton>
                  </div>
                )}
                {activeKey === 3 && (
                  <div>
                    {" "}
                    <CRow style={{ paddingBottom: 10 }}>
                      <CCol sm={2}> Donation(s)</CCol>
                      <CCol>
                        {" "}
                        <CButton size="sm" color="info">
                          Add Receipt
                        </CButton>
                      </CCol>
                    </CRow>
                    <CCol style={table}>
                      <CRow>
                        <CCol style={column}>Payment Receipt</CCol>
                        <CCol style={column}>Amount</CCol>
                      </CRow>
                      <CRow>
                        <CCol style={column}>
                          <CFormGroup>
                            <CInputFile
                              id="filePath"
                              name="filePath"
                              type="file"
                              onChange={(event) =>
                                this.setState({
                                  filePath: event.target.files[0],
                                })
                              }
                            />
                          </CFormGroup>
                        </CCol>
                        <CCol style={column}>
                          <CFormGroup>
                            <CInput
                              id="amount"
                              type="number"
                              name="amount"
                              value={amount}
                              onChange={this.handleChange}
                            />
                          </CFormGroup>
                        </CCol>
                      </CRow>
                      <CRow>
                        <CCol style={column}>Total</CCol>
                        <CCol style={column}>RM 0.00</CCol>
                      </CRow>
                    </CCol>
                    <div style={{ paddingTop: 20 }}>
                      <CButton
                        color="secondary"
                        onClick={this.previousButton.bind(this)}
                        className="float-left"
                      >
                        Previous
                      </CButton>
                      <CButton
                        color="info"
                        onClick={this.nextButton.bind(this)}
                        className="float-right"
                      >
                        Next
                      </CButton>
                    </div>
                  </div>
                )}
                {activeKey === 4 && (
                  <div>
                    <CLabel>
                      Do you want us to post your tax exemption receipt ?
                    </CLabel>
                    <CFormGroup row>
                      <CCol>
                        {this.state.posts.map((post) => (
                          <CFormGroup variant="custom-radio" inline>
                            <CInputRadio
                              custom
                              id={post.id}
                              name="postId"
                              value={post.id}
                              key={post.id}
                              defaultChecked={post.id === 2}
                              onChange={this.handleChange}
                            />
                            <CLabel variant="custom-checkbox" htmlFor={post.id}>
                              {post.postStatus}
                            </CLabel>
                          </CFormGroup>
                        ))}
                      </CCol>
                    </CFormGroup>
                    {/* // eslint-disable-next-line */}
                    {postId === 1 ||
                      (postId === "1" && (
                        <div>
                          <CLabel>
                            Shipping Address same as address at the above ?
                          </CLabel>
                          <CFormGroup row>
                            <CCol>
                              <CFormGroup variant="custom-radio" inline>
                                <CInputRadio
                                  custom
                                  id="shippingId1"
                                  name="shippingId"
                                  value="true"
                                  onChange={this.handleChecked}
                                />
                                <CLabel
                                  variant="custom-checkbox"
                                  htmlFor="shippingId1"
                                >
                                  Yes
                                </CLabel>
                              </CFormGroup>
                              <CFormGroup variant="custom-radio" inline>
                                <CInputRadio
                                  custom
                                  id="shippingId2"
                                  name="shippingId"
                                  defaultChecked
                                  value="false"
                                  onChange={this.handleChecked}
                                />
                                <CLabel
                                  variant="custom-checkbox"
                                  htmlFor="shippingId2"
                                >
                                  No
                                </CLabel>
                              </CFormGroup>
                            </CCol>
                          </CFormGroup>
                          {shippingId === false && (
                            <div>
                              {" "}
                              <CFormGroup>
                                <CLabel htmlFor="address">
                                  Shipping Address
                                </CLabel>
                                <CInput
                                  id="shippingAddress1"
                                  placeholder="Shipping Address Line 1"
                                  name="shippingAddress1"
                                  value={shippingAddress1}
                                  onChange={this.handleChange}
                                />
                                <br></br>
                                <CInput
                                  id="shippingAddress2"
                                  placeholder="Shipping Address Line 2"
                                  name="shippingAddress2"
                                  value={shippingAddress2}
                                  onChange={this.handleChange}
                                />
                              </CFormGroup>
                              <CFormGroup row className="my-0">
                                <CCol sm="4">
                                  <CFormGroup>
                                    <CLabel htmlFor="city">City</CLabel>
                                    <CInput
                                      id="shippingCity"
                                      name="shippingCity"
                                      placeholder="City"
                                      value={shippingCity}
                                      onChange={this.handleChange}
                                    />
                                  </CFormGroup>
                                </CCol>
                                <CCol sm="4">
                                  <CFormGroup>
                                    <CLabel htmlFor="shippingState">
                                      State
                                    </CLabel>
                                    <CSelect
                                      name="shippingStateId"
                                      id="shippingState"
                                      value={shippingStateId}
                                      onChange={this.handleChange}
                                    >
                                      <option value="0">Please select</option>
                                      {this.state.states.map((state) => (
                                        <option
                                          value={state.id}
                                          key={`state${state.id}`}
                                        >
                                          {" "}
                                          {state.state}
                                        </option>
                                      ))}
                                    </CSelect>
                                  </CFormGroup>
                                </CCol>
                                <CCol sm="4">
                                  <CFormGroup>
                                    <CLabel htmlFor="postalCode">
                                      Postal Code
                                    </CLabel>
                                    <CInput
                                      id="shippingPostalCode"
                                      placeholder="Postal Code"
                                      name="shippingPostalCode"
                                      value={shippingPostalCode}
                                      onChange={this.handleChange}
                                    />
                                  </CFormGroup>
                                </CCol>
                              </CFormGroup>
                            </div>
                          )}
                        </div>
                      ))}
                    <CButton
                      color="secondary"
                      onClick={this.previousButton.bind(this)}
                      className="float-left"
                    >
                      Previous
                    </CButton>
                    <div className="float-right">
                      <CButton
                        type="cancel"
                        color="secondary"
                        onClick={this.modalForm.bind(this)}
                      >
                        {" "}
                        Cancel
                      </CButton>{" "}
                      <CButton
                        type="submit"
                        color="info"
                        onClick={this.submitForm.bind(this)}
                      >
                        {" "}
                        Submit
                      </CButton>
                    </div>
                  </div>
                )}
              </CCol>
            </CRow>
          </CCol>
        </CRow>
      </div>
    );
  }
}

export default TaxRequestForm;
