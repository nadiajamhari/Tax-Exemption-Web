import React from "react";
import { CButton } from "@coreui/react";

const RequestButton = () => {
  return (
    <CButton
      size="md"
      className="mx-2 my-2"
      color="info"
      variant="outline"
      to="/request-tax-exemption"
    >
      Request Tax Exemption
    </CButton>
  );
};

export default RequestButton;
