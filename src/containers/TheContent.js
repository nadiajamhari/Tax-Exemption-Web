import React, { Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { CContainer, CFade, CRow, CCol } from "@coreui/react";
import Breadcrumbs from "../containers/TheBreadcrumb";
import Link from "../containers/TheLink";

// routes config
import routes from "../routes";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

const TheContent = () => {
  return (
    <main className="c-main">
      <CContainer fluid>
        <CRow className="justify-content-center mb-5" md={{ gutterX: 5 }}>
          <CCol md={3}>
            <Link />
          </CCol>
          <CCol md={6}>
            <Breadcrumbs />
            <Suspense fallback={loading}>
              <Switch>
                {routes.map((route, idx) => {
                  return (
                    route.component && (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={(props) => (
                          <CFade>
                            <route.component {...props} />
                          </CFade>
                        )}
                      />
                    )
                  );
                })}
                <Redirect from="/" to="/tax-exemption" />
              </Switch>
            </Suspense>
          </CCol>
        </CRow>
      </CContainer>
    </main>
  );
};

export default React.memo(TheContent);
