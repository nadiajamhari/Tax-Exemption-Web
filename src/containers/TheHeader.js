import React from "react";
import { CJumbotron } from "@coreui/react";

function TheHeader() {
  return (
    <div>
      <CJumbotron
        style={{
          background: "linear-gradient(to bottom, #003366 0%, #0099cc 100%)",
        }}
      >
        <h1 className="text-center" style={{ color: "white" }}>
          TAX EXEMPTION
        </h1>
      </CJumbotron>
    </div>
  );
}
export default TheHeader;
